
package com.ossjk.core.vo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页类
 * 
 * @author chair
 *
 * @param <E>
 */
public class Page<E> {
	private static final int NO_SQL_COUNT = -1;
	private static final int SQL_COUNT = 0;
	/**
	 * 页码
	 */
	private int pageNum = 1;
	/**
	 * 每页大小
	 */
	private int pageSize = 2;
	/**
	 * 开始行
	 */
	private int startRow;
	/**
	 * 结束行
	 */
	private int endRow;
	/**
	 * 总数
	 */
	private long total;
	/**
	 * 总页数
	 */
	private int pages;
	/**
	 * 数据
	 */
	private List<E> records;
	/**
	 * 参数
	 */
	private Map<String, Object> prams = new HashMap();

	public static int getNoSqlCount() {
		return NO_SQL_COUNT;
	}

	public static int getSqlCount() {
		return SQL_COUNT;
	}

	public Page(int pageNum, int pageSize) {
		this(pageNum, pageSize, SQL_COUNT);
	}

	public Page() {
		this.pageNum = 1;
		this.pageSize = 2;
	}

	public Page(int pageNum, int pageSize, boolean count) {
		this(pageNum, pageSize, count ? Page.SQL_COUNT : Page.NO_SQL_COUNT);
	}

	public Page(int pageNum, int pageSize, int total) {
		this.pageNum = pageNum;
		this.pageSize = pageSize;
		this.total = total;
	}

	public List<E> getRecords() {
		return records;
	}

	public void setRecords(List<E> records) {
		this.records = records;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getStartRow() {
		return startRow;
	}

	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
		if (this.pageSize > 0) {
			this.pages = (int) (total / this.pageSize + ((total % this.pageSize == 0) ? 0 : 1));
		} else {
			this.pages = (int) total;
		}
	}

	public void init() {
		this.startRow = pageNum > 0 ? (pageNum - 1) * pageSize : 0;
		this.endRow = pageNum * pageSize;
	}

	public boolean isCount() {
		return this.total > NO_SQL_COUNT;
	}

	public Map<String, Object> getPrams() {
		return prams;
	}

	public void setPrams(Map<String, Object> prams) {
		this.prams = prams;
	}

	@Override
	public String toString() {
		return "Page [pageNum=" + pageNum + ", pageSize=" + pageSize + ", startRow=" + startRow + ", endRow=" + endRow + ", total=" + total + ", pages=" + pages + ", records=" + records + ", prams=" + prams + "]";
	}

}