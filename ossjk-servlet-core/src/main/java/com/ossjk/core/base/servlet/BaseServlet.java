package com.ossjk.core.base.servlet;

import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;
import com.ossjk.core.util.CommonUtil;
import com.ossjk.core.util.ReflectUtil;
import com.ossjk.core.vo.AjaxReturn;

/**
 * 基础Servlet
 * 
 * @author chair
 *
 */
public class BaseServlet extends HttpServlet {

	/**
	 * 日志
	 */
	protected Logger logger = Logger.getLogger(this.getClass());

	public static final String TEXT_TYPE = "text/plain";
	public static final String JSON_TYPE = "application/json";
	public static final String XML_TYPE = "text/xml";
	public static final String HTML_TYPE = "text/html";
	public static final String JS_TYPE = "text/javascript";
	public static final String EXCEL_TYPE = "application/vnd.ms-excel";
	public static final long ONE_YEAR_SECONDS = 60 * 60 * 24 * 365;

	public static final String HEADER_ENCODING = "encoding";
	public static final String HEADER_NOCACHE = "no-cache";
	public static final String DEFAULT_ENCODING = "UTF-8";
	public static final boolean DEFAULT_NOCACHE = true;

	/**
	 * 设置相应头
	 * 
	 * @param resp
	 * @param contentType
	 * @param headers
	 *            例子 Cache-Control:no-cache, no-store, max-age=0
	 */
	public void initResponseHeader(HttpServletResponse resp, String contentType, String... headers) {
		String encoding = DEFAULT_ENCODING;
		boolean noCache = DEFAULT_NOCACHE;
		for (String header : headers) {
			String headerName = StringUtils.substringBefore(header, ":");
			String headerValue = StringUtils.substringAfter(header, ":");

			if (StringUtils.equalsIgnoreCase(headerName, HEADER_ENCODING)) {
				encoding = headerValue;
			} else if (StringUtils.equalsIgnoreCase(headerName, HEADER_NOCACHE)) {
				noCache = Boolean.parseBoolean(headerValue);
			} else {
				throw new IllegalArgumentException(headerName + "类型不对！");
			}
		}
		String fullContentType = contentType + ";charset=" + encoding;
		resp.setContentType(fullContentType);
		if (noCache) {
			setNoCacheHeader(resp);
		}
	}

	/**
	 * 设置没有缓存响应头
	 * 
	 * @param response
	 * 
	 */
	public void setNoCacheHeader(HttpServletResponse response) {
		response.setDateHeader("Expires", 0);
		response.addHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
	}

	public void render(HttpServletResponse resp, String contentType, String content, String... headers) {
		initResponseHeader(resp, contentType, headers);
		try {
			resp.getWriter().write(content);
			resp.getWriter().flush();
			resp.getWriter().close();
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}

	/**
	 * 输出文本
	 * 
	 * @param resp
	 * @param text
	 * @param headers
	 */
	public void renderText(HttpServletResponse resp, String text, String... headers) {
		render(resp, BaseServlet.TEXT_TYPE, text, headers);
	}

	/**
	 * 输出html
	 * 
	 * @param resp
	 * @param text
	 * @param headers
	 */
	public void renderHtml(HttpServletResponse resp, String html, String... headers) {
		render(resp, BaseServlet.HTML_TYPE, html, headers);
	}

	/**
	 * 输出json
	 * 
	 * @param resp
	 * @param text
	 * @param headers
	 */
	public void renderJson(HttpServletResponse resp, String jsonString, String... headers) {
		render(resp, BaseServlet.JSON_TYPE, jsonString, headers);
	}

	/**
	 * 输出Ajax返回类型
	 * 
	 * @param resp
	 * @param ajaxReturn
	 * @param headers
	 */
	public void renderAjaxReturn(HttpServletResponse resp, AjaxReturn ajaxReturn, String... headers) {
		render(resp, BaseServlet.JSON_TYPE, JSON.toJSONString(ajaxReturn), headers);
	}

	/**
	 * 重定向
	 * 
	 * @param request
	 * @param response
	 * @param path
	 */
	public void redirect(HttpServletRequest request, HttpServletResponse response, String path) {
		try {
			response.sendRedirect(path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 跳转
	 * 
	 * @param request
	 * @param response
	 * @param path
	 */
	public void forward(HttpServletRequest request, HttpServletResponse response, String path) {
		try {
			request.getRequestDispatcher(path).forward(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getMethod(HttpServletRequest request) {
		return request.getParameter("method");
	}

	/**
	 * 根据表单封装对象
	 * 
	 * @param class1
	 *            对象类型
	 * @param request
	 *            请求对象
	 * @return
	 * @throws Exception
	 */
	public <T> T getParamtoObject(Class<T> class1, HttpServletRequest request) throws Exception {
		T model = null;
		Field[] fields = class1.getDeclaredFields();
		if (!CommonUtil.isBlank(fields)) {
			model = class1.newInstance();
			for (Field field : fields) {
				String type = field.getType().getName();
				if (!CommonUtil.isBlank(request.getParameter(field.getName()))) {
					if (type.endsWith("String")) {
						ReflectUtil.setFieldValue(model, field.getName(), request.getParameter(field.getName()));
					} else if (type.endsWith("double") || type.endsWith("Double")) {
						ReflectUtil.setFieldValue(model, field.getName(), new BigDecimal(request.getParameter(field.getName())).doubleValue());
					} else if (type.endsWith("float") || type.endsWith("Float")) {
						ReflectUtil.setFieldValue(model, field.getName(), new BigDecimal(request.getParameter(field.getName())).floatValue());
					} else if (type.endsWith("short") || type.endsWith("Short")) {
						ReflectUtil.setFieldValue(model, field.getName(), new BigDecimal(request.getParameter(field.getName())).shortValue());
					} else if (type.endsWith("long") || type.endsWith("Long")) {
						ReflectUtil.setFieldValue(model, field.getName(), new BigDecimal(request.getParameter(field.getName())).longValue());
					} else if (type.endsWith("int") || type.endsWith("Integer")) {
						ReflectUtil.setFieldValue(model, field.getName(), new BigDecimal(request.getParameter(field.getName())).intValue());
					} else if (type.endsWith("Date")) {
						ReflectUtil.setFieldValue(model, field.getName(), CommonUtil.getDate(request.getParameter(field.getName()), "@ISO6"));
					} else if (type.endsWith("BigDecimal")) {
						ReflectUtil.setFieldValue(model, field.getName(), new BigDecimal(request.getParameter(field.getName())));
					}
				}

			}
		}
		return model;
	}

	/**
	 * 获取Ip地址
	 * 
	 * @return
	 */
	protected String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip;
	}

	public void ErrorHandler(HttpServletRequest request, HttpServletResponse response) {
		if (isAjaxRequest(request)) {
			AjaxReturn ajaxReturn = new AjaxReturn();
			ajaxReturn.setCode("1002");
			ajaxReturn.setMsg("程序出错");
			renderAjaxReturn(response, ajaxReturn);
		} else {
			redirect(request, response, request.getContextPath() + "/admin/system/index.do?method=toError");
		}
	}

	/**
	 * 是否是Ajax异步请求
	 * 
	 * @param request
	 * @return
	 */
	public static boolean isAjaxRequest(HttpServletRequest request) {
		String accept = request.getHeader("accept");
		if (accept != null && accept.indexOf("application/json") != -1) {
			return true;
		}

		String xRequestedWith = request.getHeader("X-Requested-With");
		if (xRequestedWith != null && xRequestedWith.indexOf("XMLHttpRequest") != -1) {
			return true;
		}
		return false;
	}

}
