package com.ossjk.core.base.dao;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.log4j.Logger;

import com.alibaba.druid.pool.DruidDataSource;
import com.ossjk.core.util.CommonUtil;
import com.ossjk.core.util.ReflectUtil;
import com.ossjk.core.vo.Page;

/**
 * 基础的Dao类（所有dao的父类）
 * 
 * @author chair
 *
 */
public class BaseDao<T> {

	private Connection connection = null;
	private static Logger logger = Logger.getLogger(BaseDao.class);
	private static String prefix = "";
	private static final String dialect = "mysql";
	private DruidPool druidPool;
	private DruidDataSource druidDataSource;

	public BaseDao() {
		druidDataSource = druidPool.getInstance().getDataSource();
	}

	/**
	 * 获取连接
	 * 
	 * @return
	 */
	public Connection getConnection() {
		try {
			logger.debug("获取数据库连接。。。");
			if (connection == null || connection.isClosed()) {
				// 创建连接
				connection = druidDataSource.getConnection();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("获取连接失败。");
		}
		return connection;
	}

	/**
	 * 关闭连接
	 * 
	 * @return
	 */
	public Connection closeConnection() {
		try {
			logger.debug("回收数据库连接。。。");
			if (connection != null && !connection.isClosed()) {
				// 关闭连接
				connection.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error("关闭连接失败。");
		}
		return connection;
	}

	protected ResultSet query(String sql, Object... parameters) throws SQLException {
		ResultSet resultSet = null;
		PreparedStatement prepareStatement = getConnection().prepareStatement(sql);
		// 动态填充占位符
		if (parameters != null && parameters.length > 0) {
			for (int i = 0; i < parameters.length; i++) {
				if (!CommonUtil.isBlank(parameters[i])) {
					prepareStatement.setObject(i + 1, parameters[i]);
				} else {
					logger.warn(i + "下标参数为null。");
				}

			}
		}
		// 执行sql
		logger.debug("执行:" + sql);
		logger.debug("参数:" + Arrays.toString(parameters));
		resultSet = prepareStatement.executeQuery();
		return resultSet;

	}

	protected int meger(String sql, Object... parameters) throws SQLException {

		int result = 0;
		PreparedStatement prepareStatement = getConnection().prepareStatement(sql);
		// 动态填充占位符
		if (parameters != null && parameters.length > 0) {
			for (int i = 0; i < parameters.length; i++) {
				prepareStatement.setObject(i + 1, parameters[i]);
			}
		}
		logger.debug("执行:" + sql);
		logger.debug("参数:" + Arrays.toString(parameters));
		result = prepareStatement.executeUpdate();
		return result;
	}

	/**
	 * 动态调用setter方法
	 * 
	 * @param resultSet
	 * @return
	 */
	public <R> R auttoSetter(ResultSet resultSet, Class<R> clazz) {
		try {
			Class pojoClass = clazz;
			// 新建一对象
			Object pojoObj = pojoClass.newInstance();
			//// 通过反射获取到实体类里面的所有属性
			Field[] fields = clazz.getDeclaredFields();
			Class parent = clazz.getSuperclass();
			while (parent != Object.class) {
				fields = ArrayUtils.addAll(fields, parent.getDeclaredFields());
				parent = parent.getSuperclass();
			}
			if (fields != null && fields.length > 0) {
				for (int i = 0; i < fields.length; i++) {
					String type = fields[i].getType().getName();
					String fieldName = fields[i].getName();
					if (isExistColumn(resultSet, fieldName)) {
						// 字段名字一致
						// 动态封装
						if (type.lastIndexOf("Integer") > -1 || type.lastIndexOf("int") > -1) {
							ReflectUtil.invokeSetter(pojoObj, fieldName, resultSet.getInt(fieldName));
						} else if (type.lastIndexOf("Long") > -1 || type.lastIndexOf("long") > -1) {
							ReflectUtil.invokeSetter(pojoObj, fieldName, resultSet.getLong(fieldName));
						} else if (type.lastIndexOf("Short") > -1 || type.lastIndexOf("short") > -1) {
							ReflectUtil.invokeSetter(pojoObj, fieldName, resultSet.getShort(fieldName));
						} else if (type.lastIndexOf("Float") > -1 || type.lastIndexOf("float") > -1) {
							ReflectUtil.invokeSetter(pojoObj, fieldName, resultSet.getFloat(fieldName));
						} else if (type.lastIndexOf("Double") > -1 || type.lastIndexOf("double") > -1) {
							ReflectUtil.invokeSetter(pojoObj, fieldName, resultSet.getDouble(fieldName));
						} else if (type.lastIndexOf("String") > -1) {
							ReflectUtil.invokeSetter(pojoObj, fieldName, resultSet.getString(fieldName));
						} else if (type.lastIndexOf("Date") > -1) {
							ReflectUtil.invokeSetter(pojoObj, fieldName, resultSet.getDate(fieldName));
						} else if (type.lastIndexOf("BigDecimal") > -1) {
							ReflectUtil.invokeSetter(pojoObj, fieldName, resultSet.getBigDecimal(fieldName));
						}
					}
				}
				return (R) pojoObj;
			}
		} catch (Exception e) {
			logger.error("自动注入错误。");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 判断查询结果集中是否存在某列
	 * 
	 * @param rs
	 *            查询结果集
	 * @param columnName
	 *            列名
	 * @return true 存在; false 不存在
	 */
	private boolean isExistColumn(ResultSet resultSet, String columnName) {
		try {
			if (resultSet.findColumn(columnName) > 0) {
				return true;
			}
		} catch (SQLException e) {
			return false;
		}

		return false;
	}

	/**
	 * 根据泛型获取类
	 * 
	 * @return
	 */
	private Class getPojoClass() {
		// 根据反射获取到泛型类型
		Class c = this.getClass();
		Type t = c.getGenericSuperclass();
		if (t instanceof ParameterizedType) {
			Type[] p = ((ParameterizedType) t).getActualTypeArguments();
			return (Class<T>) p[0];
		}
		return null;
	}

	/**
	 * 获取主键
	 * 
	 * @return
	 */
	public String getPrimaryKey() {
		String primaryKey = "id";
		Class pojoClass = getPojoClass();
		Field[] fields = pojoClass.getDeclaredFields();
		if (fields != null && fields.length > 0) {
			for (Field field : fields) {
				if (field.getAnnotation(ID.class) != null) {
					primaryKey = field.getName();
				}
			}
		}
		return primaryKey;
	}

	/**
	 * 获取表名
	 * 
	 * @return
	 */
	public String getTableName() {
		return prefix + getPojoClass().getSimpleName();
	}

	/**
	 * 查找多行
	 * 
	 * @param sql
	 * @param parameters
	 * @return
	 * @throws SQLException
	 */
	public List<T> selectSome(String sql, Object... parameters) throws SQLException {
		List<T> list = null;
		ResultSet resultSet = query(sql, parameters);
		while (resultSet.next()) {
			if (list == null) {
				list = new ArrayList();
			}
			T t = (T) auttoSetter(resultSet, getPojoClass());
			list.add(t);
		}
		closeConnection();

		return list;
	}

	/**
	 * 查找多行
	 * 
	 * @param class1
	 * @param sql
	 * @param parameters
	 * @return
	 * @throws SQLException
	 */
	public <R> List<R> selectSome(Class<R> class1, String sql, Object... parameters) throws SQLException {
		List<R> list = null;
		ResultSet resultSet = query(sql, parameters);
		while (resultSet.next()) {
			if (list == null) {
				list = new ArrayList();
			}
			R r = auttoSetter(resultSet, class1);
			list.add(r);
		}
		closeConnection();
		return list;
	}

	/**
	 * 查找单行
	 * 
	 * @param class1
	 * @param sql
	 * @param parameters
	 * @return
	 * @throws SQLException
	 */
	public <R> R selectOne(Class<R> class1, String sql, Object... parameters) throws SQLException {
		R r = null;
		ResultSet resultSet = query(sql, parameters);
		if (resultSet.next()) {
			r = auttoSetter(resultSet, class1);
		}
		closeConnection();
		return r;
	}

	/**
	 * 查找单行
	 * 
	 * @param sql
	 * @param parameters
	 * @return
	 * @throws SQLException
	 */
	public T selectOne(String sql, Object... parameters) throws SQLException {
		T t = null;
		ResultSet resultSet = query(sql, parameters);
		if (resultSet.next()) {
			t = (T) auttoSetter(resultSet, getPojoClass());
		}
		closeConnection();
		return t;
	}

	public String getSelectAllSql() {
		return "select * from " + getTableName() + " ";
	}

	/**
	 * 获取更新sql
	 * 
	 * @return
	 */
	public String getUpdateSql() {
		StringBuffer sql = new StringBuffer("update " + getTableName() + " set ");
		Field[] fields = getPojoClass().getDeclaredFields();
		for (int i = 1; i < fields.length; i++) {
			sql.append(fields[i].getName() + " = ?");
			if (i < fields.length - 1) {
				sql.append(",");
			}
		}
		sql.append(" where " + getPrimaryKey() + " = ?");
		return sql.toString();
	}

	/**
	 * 获取新增sql
	 * 
	 * @return
	 */
	public String getInsertSql() {
		StringBuffer sql = new StringBuffer("insert into " + getTableName() + "( ");
		Class entityClass = ReflectUtil.getClassGenricType(this.getClass());
		Field[] fields = entityClass.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			sql.append(fields[i].getName());
			if (i < fields.length - 1) {
				sql.append(",");
			}
		}
		sql.append(" ) values( ");
		for (int i = 0; i < fields.length; i++) {
			sql.append("?");
			if (i < fields.length - 1) {
				sql.append(",");
			}
		}
		sql.append(")");
		return sql.toString();
	}

	/**
	 * 通用查詢所有方法
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<T> selectAll() throws SQLException {
		String sql = getSelectAllSql();
		return selectSome(sql);
	}

	/**
	 * 分页查找所有
	 * 
	 * @param page
	 * @return
	 * @throws SQLException
	 */
	public List<T> selectAllByPage(Page<T> page) throws SQLException {
		return selectSome(getPageSql(getSelectAllSql(), page));
	}

	/**
	 * 分页查找
	 * 
	 * @param class1
	 * @param sql
	 * @param page
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public <R> List<R> selectByPage(Class<R> class1, String sql, Page<R> page, Object... parameters) throws SQLException {
		return selectSome(class1, getPageSql(sql, page), parameters);
	}

	/**
	 * 分页查找分页信息
	 * 
	 * @param sql
	 * @param page
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public Page<T> selectByPageCount(String sql, Page<T> page, Object... parameters) throws SQLException {
		Integer total = selectCount(sql, parameters);
		List records = selectSome(getPageSql(sql, page), parameters);
		page.setTotal(total);
		page.setRecords(records);
		return page;
	}

	/**
	 * 分页查找分页信息
	 * 
	 * @param class1
	 * @param sql
	 * @param page
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public <R> Page<R> selectByPageCount(Class<R> class1, String sql, Page<R> page, Object... parameters) throws SQLException {
		Integer total = selectCount(sql, parameters);
		List records = selectSome(class1, getPageSql(sql, page), parameters);
		page.setTotal(total);
		page.setRecords(records);
		return page;
	}

	/**
	 * 查找总数
	 * 
	 * @return
	 * @throws SQLException
	 * 
	 */
	public Integer selectCount(String sql, Object... parameters) throws SQLException {
		int count = 0;
		String countSql = getCountSql(sql);
		ResultSet rs = query(countSql, parameters);
		if (rs != null) {
			if (rs.next()) {
				count = rs.getInt(1);
			}
		}
		closeConnection();
		return count;
	}

	/**
	 * 获取总数sql - 如果要支持其他数据库，修改这里就可以
	 *
	 * @param sql
	 * @return
	 */
	private String getCountSql(String sql) {
		return "select count(0) from (" + sql + ") tmp_count";
	}

	/**
	 * 分页查找
	 * 
	 * @param sql
	 * @param page
	 * @param obj
	 * @return
	 * @throws SQLException
	 */
	public List<T> selectByPage(String sql, Page<T> page, Object... obj) throws SQLException {
		return selectSome(getPageSql(sql, page), obj);
	}

	/**
	 * 通用根据id查找
	 * 
	 * @return
	 * @throws SQLException
	 */
	public T selectById(Serializable id) throws SQLException {
		String sql = getSelectAllSql() + " where " + getPrimaryKey() + " = ? ";
		return selectOne(sql, id);
	}

	/**
	 * 通用删除方法
	 * 
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public int delete(Serializable id) throws SQLException {
		int result = 0;
		// 3. 创建状态
		String sql = "delete from " + getTableName() + "  where " + getPrimaryKey() + " = ? ";
		// 4. 执行sql
		result = meger(sql, id);
		// 5. 关闭连接
		closeConnection();
		return result;
	}

	/**
	 * 通用更新
	 * 
	 * @param t
	 * @return
	 * @throws SQLException
	 */
	public int update(T t) throws SQLException {
		int result = 0;
		// 生成sql
		String sql = getUpdateSql();
		// 通过反射动态获取更新参数
		Class pojoClass = getPojoClass();
		Field[] fields = pojoClass.getDeclaredFields();
		if (!CommonUtil.isBlank(fields)) {
			List<Object> parameters = new ArrayList();
			for (int i = 0; i < fields.length; i++) {
				if (!fields[i].getName().equals(getPrimaryKey())) {
					parameters.add(ReflectUtil.getFieldValue(t, fields[i].getName()));
				}
			}
			Object id = ReflectUtil.getFieldValue(t, getPrimaryKey());
			if (id != null && !"".equals(id.toString())) {
				parameters.add(id);
				result = meger(getUpdateSql(), parameters.toArray());
			}
			// 5. 关闭连接
			closeConnection();
		}
		return result;
	}

	/**
	 * 通用新增
	 * 
	 * @param t
	 * @return
	 * @throws SQLException
	 */
	public int insert(T t) throws SQLException {
		int result = 0;
		String sql = getInsertSql();
		Field[] fields = getPojoClass().getDeclaredFields();
		List parameters = new ArrayList();
		for (int i = 0; i < fields.length; i++) {
			parameters.add(ReflectUtil.getFieldValue(t, fields[i].getName()));
		}
		result = meger(sql, parameters.toArray());
		// 5. 关闭连接
		closeConnection();
		return result;
	}

	/**
	 * 批量删除
	 * 
	 * @param id
	 * @return
	 */
	public Integer batchDelete(Serializable[] id) throws SQLException {
		int result = 0;
		if (!CommonUtil.isBlank(id)) {
			StringBuilder sql = new StringBuilder();
			sql.append("delete from " + getTableName() + " where " + getPrimaryKey() + " in (");
			for (Serializable serializable : id) {
				sql.append(" ? ,");
			}
			sql.append(" )");
			int lastIndexOf = sql.toString().lastIndexOf(",");
			sql = sql.replace(lastIndexOf, lastIndexOf + ",".length(), "");
			result = meger(sql.toString(), id);
		}
		// 5. 关闭连接
		closeConnection();
		return result;
	}

	/**
	 * 选择更新
	 * 
	 * @param t
	 * @return
	 * @throws SQLException
	 */
	public Integer updateBySelected(T t) throws SQLException {
		int result = 0;
		Field[] fields = getPojoClass().getDeclaredFields();
		List params = new ArrayList();
		if (fields != null && fields.length > 0) {
			StringBuffer sql = new StringBuffer("update " + getTableName() + " set ");
			boolean flag = false;
			for (int i = 1; i < fields.length; i++) {
				if (!fields[i].getName().equals(getPrimaryKey())) {
					Object value = ReflectUtil.getFieldValue(t, fields[i].getName());
					if (value != null) {
						flag = true;
						params.add(value);
						sql.append(fields[i].getName() + " = ?");
						sql.append(",");
					}
				}
			}
			sql.append(" where " + getPrimaryKey() + " = ?");
			int lastIndexOf = sql.toString().lastIndexOf(",");
			sql = sql.replace(lastIndexOf, lastIndexOf + ",".length(), "");
			Object id = ReflectUtil.getFieldValue(t, getPrimaryKey());
			if (id != null && !"".equals(id.toString())) {
				params.add(id);
				if (flag) {
					result = meger(sql.toString(), params.toArray());
				}
			}
		}
		// 5. 关闭连接
		closeConnection();
		return result;

	}

	/**
	 * 获取分页sql
	 * 
	 * @param sql
	 * @param page
	 * @return
	 */
	private String getPageSql(String sql, Page page) {
		page.init();
		StringBuilder pageSql = new StringBuilder();
		if ("mysql".equals(dialect)) {
			pageSql.append("select * from (");
			pageSql.append(sql);
			pageSql.append(") as tmp_page limit " + page.getStartRow() + "," + page.getPageSize());
		} else if ("hsqldb".equals(dialect)) {
			pageSql.append(sql);
			pageSql.append(" LIMIT " + page.getPageSize() + " OFFSET " + page.getStartRow());
		} else if ("oracle".equals(dialect)) {
			pageSql.append("select * from ( select temp.*, rownum row_id from ( ");
			pageSql.append(sql);
			pageSql.append(" ) temp where rownum <= ").append(page.getEndRow());
			pageSql.append(") where row_id > ").append(page.getStartRow());
		}
		return pageSql.toString();
	}

	/**
	 * 生成实体类
	 * 
	 * @throws Exception
	 */
	public void generatePojoTitle(String tablename) throws Exception {
		StringBuilder title = new StringBuilder("import java.io.Serializable;\n\n");
		title.append("public class ");
		String entityName = tablename.substring(0, 1).toUpperCase() + tablename.substring(1, tablename.length()).toLowerCase();
		title.append(entityName);
		title.append(" implements Serializable {\n\n");
		Connection connection = getConnection();
		ResultSet rs = connection.prepareStatement("show full fields from " + tablename).executeQuery();
		String tpyeName = null;
		String comment = null;
		String field = null;
		while (rs.next()) {
			comment = rs.getString("comment");
			field = rs.getString("field");
			tpyeName = rs.getString("type").substring(0, 3);
			title.append("\t/**\n");
			title.append("\t*" + comment + " \n");
			title.append("\t*/\n");
			if (rs.getString("key").equals("PRI")) {
				title.append("\t@ID\n");
			}
			title.append("\tprivate ");
			if ("dec".equals(tpyeName) || "num".equals(tpyeName)) {
				title.append(" BigDecimal ");
			} else if ("int".equals(tpyeName) || "sma".equals(tpyeName) || "tin".equals(tpyeName) || "big".equals(tpyeName)) {
				title.append(" Integer ");
			} else if ("dou".equals(tpyeName) || "flo".equals(tpyeName)) {
				title.append(" Double ");
			} else if ("var".equals(tpyeName) || "cha".equals(tpyeName) || "tex".equals(tpyeName)) {
				title.append(" String ");
			} else if ("dat".equals(tpyeName) || "tim".equals(tpyeName) || "yea".equals(tpyeName)) {
				title.append(" Date ");
			} else {
				title.append(" Object ");
			}
			title.append(field + ";");
			title.append("\n");

		}
		title.append("\n");
		title.append("}");
		System.out.println(title.toString());
		connection.close();
	}

	/**
	 * 生成实体类
	 * 
	 * @throws Exception
	 */
	public void generateDaoTitle(String tablename) throws Exception {
		StringBuilder title = new StringBuilder();
		title.append("public class ");
		String entityName = tablename.substring(0, 1).toUpperCase() + tablename.substring(1, tablename.length()).toLowerCase();
		String daoName = tablename.substring(0, 1).toUpperCase() + tablename.substring(1, tablename.length()).toLowerCase() + "Dao";
		title.append(daoName + " extends BaseDao<" + entityName + ">  {\n\n");
		title.append("\n");
		title.append("}");
		System.out.println(title.toString());
	}

	public static void main(String[] args) {
		BaseDao baseDao = new BaseDao();
		try {
			baseDao.generatePojoTitle("user");
			baseDao.generateDaoTitle("user");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
