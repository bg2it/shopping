package com.ossjk.ossjkservlet.admin.system.servlet;

import java.sql.SQLException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ossjk.core.base.servlet.BaseServlet;
import com.ossjk.core.util.CommonUtil;
import com.ossjk.ossjkservlet.admin.system.dao.UsrDao;
import com.ossjk.ossjkservlet.admin.system.pojo.Usr;
import com.ossjk.ossjkservlet.common.Constant;

@WebServlet("/admin/system/index.do")
public class IndexServlet extends BaseServlet {

	private UsrDao usrDao = new UsrDao();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		try {
			String method = getMethod(req);
			if ("toIndex".equals(method)) {
				toIndex(req, resp);
			} else if ("toLogin".equals(method)) {
				toLogin(req, resp);
			} else if ("login".equals(method)) {
				login(req, resp);
			} else if ("logout".equals(method)) {
				logout(req, resp);
			} else if ("toDesktop".equals(method)) {
				toDesktop(req, resp);
			} else if ("toError".equals(method)) {
				toError(req, resp);
			}

		} catch (Exception e) {
			logger.error("请求报错。", e);
			ErrorHandler(req, resp);
		}
	}

	/**
	 * 登出
	 * 
	 * @param req
	 * @param resp
	 */
	public void logout(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		req.getSession().removeAttribute(Constant.SESSION_USER_KEY);
		redirect(req, resp, req.getContextPath() + "/admin/system/index.do?method=toLogin");
	}

	/**
	 * 登录
	 * 
	 * @param req
	 * @param resp
	 * @throws SQLException
	 */
	public void login(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		String name = (String) req.getParameter("name");
		String pwd = (String) req.getParameter("pwd");
		Usr usr = usrDao.selectOne(usrDao.getSelectAllSql() + " where name = ?  and password = ? ", name, pwd);
		if (!CommonUtil.isBlank(usr)) {
			// 登录成功
			req.getSession().setAttribute(Constant.SESSION_USER_KEY, usr);
			redirect(req, resp, req.getContextPath() + "/admin/system/index.do?method=toIndex");
		} else {
			// 登录失败
			req.setAttribute("msg", "登录失败");
			forward(req, resp, "/WEB-INF/page/admin/login.jsp");
		}
	}

	/**
	 * 去登录
	 * 
	 * @param req
	 * @param resp
	 * @throws Exception
	 */
	public void toLogin(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		forward(req, resp, "/WEB-INF/page/admin/login.jsp");
	}

	/**
	 * 去主页
	 * 
	 * @param req
	 * @param resp
	 */
	public void toIndex(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		forward(req, resp, "/WEB-INF/page/admin/index.jsp");
	}

	/**
	 * 去桌面
	 * 
	 * @param req
	 * @param resp
	 */
	public void toDesktop(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		req.setAttribute("props", System.getProperties());
		forward(req, resp, "/WEB-INF/page/admin/desktop.jsp");
	}

	/**
	 * 去错误页
	 * 
	 * @param req
	 * @param resp
	 */
	public void toError(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		forward(req, resp, "/WEB-INF/page/admin/500.jsp");
	}

}
