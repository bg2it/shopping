package com.ossjk.ossjkservlet.admin.system.pojo;

import java.io.Serializable;
import java.util.Date;

import com.ossjk.core.base.dao.ID;

public class Usr implements Serializable {

	/**
	 * id
	 */
	@ID
	private Integer id;
	/**
	 * 姓名
	 */
	private String name;
	/**
	 * 密码
	 */
	private String password;
	/**
	 * 性别
	 */
	private Integer sex;
	/**
	 * 联系方式
	 */
	private String phone;
	/**
	 * 年龄
	 */
	private Integer age;
	/**
	 * 地址
	 */
	private String address;
	/**
	 * 生日日期
	 */
	private Date birth;
	/**
	 * 邮件
	 */
	private String email;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Usr [id=" + id + ", name=" + name + ", password=" + password + ", sex=" + sex + ", phone=" + phone + ", age=" + age + ", address=" + address + ", birth=" + birth + ", email=" + email + "]";
	}

}