package com.ossjk.ossjkservlet.admin.system.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.ossjk.core.base.servlet.BaseServlet;
import com.ossjk.core.util.CommonUtil;
import com.ossjk.core.vo.AjaxReturn;
import com.ossjk.core.vo.Page;
import com.ossjk.ossjkservlet.admin.system.dao.UsrDao;
import com.ossjk.ossjkservlet.admin.system.pojo.Usr;
import com.ossjk.ossjkservlet.common.Constant;

@WebServlet("/admin/system/usr.do")
public class UsrServlet extends BaseServlet {

	private Logger logger = Logger.getLogger(UsrServlet.class);
	private UsrDao usrDao = new UsrDao();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
		String method = getMethod(req);
		try {
			if ("list".equals(method)) {
				list(req, resp);
			} else if ("toInsert".equals(method)) {
				toInsert(req, resp);
			} else if ("insert".equals(method)) {
				insert(req, resp);
			} else if ("toUpdate".equals(method)) {
				toUpdate(req, resp);
			} else if ("update".equals(method)) {
				update(req, resp);
			} else if ("delete".equals(method)) {
				delete(req, resp);
			} else if ("batchDelete".equals(method)) {
				batchDelete(req, resp);
			}
		} catch (Exception e) {
			logger.error("请求报错。", e);
			ErrorHandler(req, resp);
		}
	}

	/**
	 * 列表
	 * 
	 * @param req
	 * @param resp
	 */
	public void list(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		Page page = getParamtoObject(Page.class, req);
		String sql = usrDao.getSelectAllSql();
		sql += " where 1 = 1 ";
		List args = new ArrayList();
		if (!CommonUtil.isBlank(req.getParameter("name"))) {
			sql += "and name like  CONCAT('%',?,'%')";
			args.add(req.getParameter("name"));
			page.getPrams().put("name", req.getParameter("name"));
		}
		req.setAttribute("page", usrDao.selectByPageCount(sql, page, args.toArray()));
		// 转发
		forward(req, resp, "/WEB-INF/page/admin/system/usr/list.jsp");

	}

	/**
	 * 去增加
	 * 
	 * @param req
	 * @param resp
	 */
	public void toInsert(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		forward(req, resp, "/WEB-INF/page/admin/system/usr/edit.jsp");
	}

	/**
	 * 去更新
	 * 
	 * @param req
	 * @param resp
	 */
	public void toUpdate(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		req.setAttribute("record", usrDao.selectById(req.getParameter("id")));
		forward(req, resp, "/WEB-INF/page/admin/system/usr/edit.jsp");
	}

	/**
	 * 增加
	 * 
	 * @param req
	 * @param resp
	 * @throws Exception
	 */
	public void insert(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		// getParamtoObject
		// 假设表单和实体类字段一致，可以调用getParamtoObject方法把表单内容自动封装到实体类当中
		Usr usr = getParamtoObject(Usr.class, req);
		// 设置生日
		usr.setBirth(CommonUtil.getDate(req.getParameter("birth"), "@ISO"));
		if (usrDao.insert(usr) > 0) {
			renderAjaxReturn(resp, new AjaxReturn(Constant.RETURN_CODE_SUCCESS, Constant.RETURN_MESSAGE_SUCCESS));
		} else {
			renderAjaxReturn(resp, new AjaxReturn(Constant.RETURN_CODE_ERROR, Constant.RETURN_MESSAGE_ERROR));
		}

	}

	/**
	 * 更新
	 * 
	 * @param req
	 * @param resp
	 * @throws Exception
	 */
	public void update(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		Usr usr = getParamtoObject(Usr.class, req);
		// 设置生日
		usr.setBirth(CommonUtil.getDate(req.getParameter("birth"), "@ISO"));
		if (usrDao.updateBySelected(usr) > 0) {
			renderAjaxReturn(resp, new AjaxReturn(Constant.RETURN_CODE_SUCCESS, Constant.RETURN_MESSAGE_SUCCESS));
		} else {
			renderAjaxReturn(resp, new AjaxReturn(Constant.RETURN_CODE_ERROR, Constant.RETURN_MESSAGE_ERROR));
		}

	}

	/**
	 * 删除
	 * 
	 * @param req
	 * @param resp
	 */
	public void delete(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		if (usrDao.delete(req.getParameter("id")) > 0) {
			renderAjaxReturn(resp, new AjaxReturn(Constant.RETURN_CODE_SUCCESS, Constant.RETURN_MESSAGE_SUCCESS));
		} else {
			renderAjaxReturn(resp, new AjaxReturn(Constant.RETURN_CODE_ERROR, Constant.RETURN_MESSAGE_ERROR));
		}

	}

	/**
	 * 批量删除
	 * 
	 * @param req
	 * @param resp
	 */
	public void batchDelete(HttpServletRequest req, HttpServletResponse resp) throws Exception {
		if (usrDao.batchDelete(req.getParameterValues("ids")) > 0) {
			renderAjaxReturn(resp, new AjaxReturn(Constant.RETURN_CODE_SUCCESS, Constant.RETURN_MESSAGE_SUCCESS));
		} else {
			renderAjaxReturn(resp, new AjaxReturn(Constant.RETURN_CODE_ERROR, Constant.RETURN_MESSAGE_ERROR));
		}

	}

}
